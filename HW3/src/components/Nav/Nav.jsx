import PropTypes from 'prop-types';

export default function Nav({ children }) {
    return (
        <nav className='navList'>
            {children}
        </nav>
    );
}

Nav.propTypes = {
    children: PropTypes.node.isRequired,
};