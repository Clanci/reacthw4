import { useState, useEffect, useContext } from 'react';
import Card from '../Cards/Card';
import { CountContext } from '../../context/countContext';
import { useSelector } from 'react-redux';

export default function Cart() {
    const [cartItems, setCartItems] = useState([]);
    const { cartCount } = useContext(CountContext);
    const itemData = useSelector(state => state.products);

    useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cartItemsFromStorage);
    }, [cartCount]);

    return (
        <section className="cartPages">
        <h2>Your Cart</h2>
        <div className="cartItems item-container">
            {Array.isArray(cartItems) ? (
                cartItems.map((sku) => {
                    const cartItemData = itemData.products.find((item) => item.sku === sku);
                    if (cartItemData) {
                        return <Card key={sku} {...cartItemData} inCart />;
                    }
                    return null; 
                })
            ) : (
                <p>No items in the cart</p>
            )}
        </div>
    </section>
    );
}