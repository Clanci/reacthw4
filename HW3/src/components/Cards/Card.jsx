import PropTypes from 'prop-types';
import Button from '../Button/Button';
import { useState, useEffect, useContext } from 'react';
import Modal from '../Modal/Modal';
import ModalClose from '../Modal/ModalClose';
import ModalHeader from '../Modal/ModalHeader';
import ModalBody from '../Modal/ModalBody';
import ModalFooter from '../Modal/ModalFooter';
import { CountContext } from '../../context/countContext';
import star1 from '../../assets/star1.png';
import star2 from '../../assets/star2.png';

import { useDispatch, useSelector } from 'react-redux';
import { setProductCartModalStatus } from '../../redux/product/productActions';

export default function Card({ name, price, image, sku, color,}){

    const {setCartCount, setFavoriteCount } = useContext(CountContext);
    const [isAddedToCart, setIsAddedToCart] = useState(false);
    const [isFavorite, setIsFavorite] = useState(false);
    const [isCartModalVisible, setCartModalVisible] = useState(
        useSelector((state) => {
                state.products.products.find((product) => product.sku === sku).isCartModalVisible;
            }
    ))

    const dispatch = useDispatch();

    
    const toggleCartModal = () => {
        setCartModalVisible(!isCartModalVisible);
        dispatch(setProductCartModalStatus({ productId: sku, isCartModalVisible: !isCartModalVisible }));
    };

    
    useEffect(() => {
        const cartItems = JSON.parse(localStorage.getItem('cart')) || [];
        const favoriteItems = JSON.parse(localStorage.getItem('favorites')) || [];
        setIsAddedToCart(cartItems.includes(sku));
        setIsFavorite(favoriteItems.includes(sku));
    }, [sku]);

    const handleAddToCart = () => {
        setIsAddedToCart((prev) => !prev);
        updateLocalStorage('cart', sku);
        toggleCartModal();
        setCartCount();
    };    
    const handleAddToFavorites = () => {
        setIsFavorite((prev) => !prev);
        updateLocalStorage('favorites', sku);
        setFavoriteCount();
    };

    const updateLocalStorage = (key, value) => {
        const items = JSON.parse(localStorage.getItem(key)) || [];
        if (items.includes(value)) {
            const updatedItems = items.filter((item) => item !== value);
            localStorage.setItem(key, JSON.stringify(updatedItems));
        } else {
            const updatedItems = [...items, value];
            localStorage.setItem(key, JSON.stringify(updatedItems));
        }
    };

    return(
    <>
        <div id={sku} className='card'>
            <img className='card_img' src={image} alt={name} />
            <div className='card_mainInfo'>
                <h2 className="card_mainInfo_name">{name}</h2>
                <p className="card_mainInfo_color">{color}</p>
                <h3 className="card_mainInfo_price">{price} ₴</h3>
            </div>
            <div className='card_btnList'>
                <Button
                    onClick={() => {
                        toggleCartModal();
                    }}
                    type='button'
                    classNames={`addToCart`}
                    >
                    {isAddedToCart ? 'Added to Cart' : 'Add to Cart'}
                </Button>
                <Button
                    onClick={() => {
                        handleAddToFavorites();
                    }}
                    type='button'
                    classNames={`starBtn`}
                >
                    {isFavorite ?  <img src={star2} alt="#" /> :  <img src={star1} alt="#" />}
                </Button>
            </div>
        </div>
        
        {isCartModalVisible && (
            <Modal
                classNames={"cartModal"}
                onClose={toggleCartModal}
                isVisible={isCartModalVisible}
            >
                <ModalClose onClick={toggleCartModal}/>
                <ModalHeader>
                    <h2>{isAddedToCart ? `Delete ${name} from cart` : `Add ${name} to cart`}</h2>
                    <p>{color}</p>
                </ModalHeader>
                <ModalBody>
                    <img className='catrModalImg' src={image} alt="" />
                </ModalBody>
                <ModalFooter
                    firstText={isAddedToCart ?"Delete":"Add"}
                    firstClick={handleAddToCart}
                    secondaryText={isAddedToCart ?"Don`t delete":"Don`t add"}
                    secondaryClick={toggleCartModal}
                >
                </ModalFooter>
            </Modal>
        )}
    </>
    )
}

Card.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    inCart: PropTypes.bool,
};